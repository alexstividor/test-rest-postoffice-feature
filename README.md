# TEST REST API post office
## created 16.04.2024

API endpoint:
* localhost:8080/api/

test GET mapping:
* curl -v localhost:8080/api/orders | json_pp

test POST
* curl -X POST -H 'Content-Type: application/json' -d @test.json localhost:8080/api/orders | json_pp

test GET mapping entity:
* curl -v localhost:8080/api/orders/1 | json_pp