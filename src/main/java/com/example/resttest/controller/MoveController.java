package com.example.resttest.controller;

import com.example.resttest.models.Move;
import com.example.resttest.models.PostalOrder;
import com.example.resttest.repository.MoveRepository;
import com.example.resttest.repository.PostalOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/moves")
public class MoveController {
    private final PostalOrderRepository orderRepository;
    private final MoveRepository moveRepository;

    @Autowired
    MoveController(PostalOrderRepository orderRepository, MoveRepository moveRepository) {
        this.orderRepository = orderRepository;
        this.moveRepository = moveRepository;
    }

    @GetMapping
    public ResponseEntity<List<Move>> all() {
        return ResponseEntity.ok(moveRepository.findAll());
    }
}
