package com.example.resttest.controller;

import com.example.resttest.models.Move;
import com.example.resttest.models.PostalOrder;
import com.example.resttest.repository.MoveRepository;
import com.example.resttest.repository.PostalOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/orders")
public class PostalOrderController {
    private final PostalOrderRepository orderRepository;
    private final MoveRepository moveRepository;

    @Autowired
    PostalOrderController(PostalOrderRepository orderRepository, MoveRepository moveRepository) {
        this.orderRepository = orderRepository;
        this.moveRepository = moveRepository;
    }

    @PostMapping
    public ResponseEntity<PostalOrder> createPostalOrder(@RequestBody PostalOrder order) {

        List<Move> moves = order.getMoves();
        for (Move move : moves) {
            Move savedMove = moveRepository.save(move);
        }
        PostalOrder savedOrder = orderRepository.save(order);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedOrder.getId()).toUri();

        return ResponseEntity.created(location).body(savedOrder);
    }

    @GetMapping
    public ResponseEntity<List<PostalOrder>> all() {
        return ResponseEntity.ok(orderRepository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PostalOrder> getById(@PathVariable Long id) {
        Optional<PostalOrder> optionalLibrary = orderRepository.findById(id);
        if (!optionalLibrary.isPresent()) {
            return ResponseEntity.unprocessableEntity().build();
        }

        return ResponseEntity.ok(optionalLibrary.get());
    }

}
