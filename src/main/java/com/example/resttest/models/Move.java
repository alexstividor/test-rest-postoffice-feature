package com.example.resttest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "moves")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Move {

    public enum Type {
        REGISTERED_AT, SENT_FROM, DELIVERED_TO, DELIVERED_TO_RECEIVER_BY
    }

    private @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;

    @Enumerated(EnumType.STRING)
    private Type type;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "postal_order_id")
    @JsonIgnore
    private PostalOrder postalOrder;

    @Column(nullable=false, unique=false)
    private String place;

   /* @Temporal(TemporalType.DATE)
    private LocalDate date;*/

}
