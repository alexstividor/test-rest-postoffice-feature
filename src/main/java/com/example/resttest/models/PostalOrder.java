package com.example.resttest.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "postal_order")
@Data
@EqualsAndHashCode(exclude = "moves")
public class PostalOrder {

    public enum Type {
        LETTER, PARCEL, BANDEROLE, POSTCARD
    }

    public enum Status {
        REGISTERED, DELIVERING, DELIVERED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String receiverName;

    @Enumerated(EnumType.STRING)
    private Type type;

    private String receiverIndex;

    private String receiverAddress;

    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "postalOrder")
    private List<Move> moves = new ArrayList<>();

   /* public void addMove(Move move) {
        moves.add(move);
        move.setPostalOrder(this);
    }*/
}

