package com.example.resttest.repository;

import com.example.resttest.models.Move;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoveRepository extends JpaRepository<Move, Long> {


}
