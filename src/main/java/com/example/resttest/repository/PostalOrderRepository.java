package com.example.resttest.repository;

import com.example.resttest.models.PostalOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostalOrderRepository extends JpaRepository<PostalOrder, Long> {
}

